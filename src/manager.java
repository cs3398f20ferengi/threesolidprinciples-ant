package threesolid; 

import threesolid.Worker;

class Manager {
	IWorker worker;

	public void setWorker(IWorker w) {
		worker=w;
	}

	public void manage() {
		worker.work();
	}
}
	//SINGLE RESPONSIBILITY - adheres to single responsibiltiy by only managing work() for each worker.
	//OPEN AND CLOSE - adheres to Open/Close as it is open to extension
	//INTERFACE SEPERATION - does not apply