package threesolid;

	// Single Responsbility, it is responsible for the worker class where it only works and eats

	// Open-close principle applies to this file because the file is open to IWorkerInterface (Thats where it pulls the functions from)
	// but it is closed to modification.
	
	// Implents both IWorker and IEat according to interface seperation principle.
	
abstract class Worker implements IWorker, IEat{
	public void work() {
		// ....working
	}

	public void eat() {
		//.... eating in launch break
	}
}