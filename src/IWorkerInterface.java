package threesolid;

interface IWorker {
	public void work();
}   

//Because of the interface-segragation principle, it is better to
//have the interface seperated from the other classes and methods 
//and to have only a work function as a part of the interface where 
//an additional class such as robot will not eat.