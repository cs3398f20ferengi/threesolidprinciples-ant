package threesolid;

interface IEat {
	public void eat();
}   

//The creation of an additional interface allows an additional class
//such as the robot class without out voilating the open/close or interface
//segregation principle