package threesolid;

// Single responsibility principle applies to this file because we only want SuperWorker to be responsible
// for itself

// Open-close principle applies to this file because the file is open to IWorkerInterface (Thats where it pulls the functions from)
// but it is closed to modification.

// Interface segregation principle uses both work and eat interfaces.

abstract class SuperWorker implements IWorker, IEat{
	public void work() {
		//.... working much more
	}

	public void eat() {
		//.... eating in launch break
	}
}