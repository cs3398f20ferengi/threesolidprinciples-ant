package threesolid; 

abstract class Robot implements IWorker{
   public void work() {
      // ....working
   }
}

//Single responsibility principle applies because the Robot class is responsible for one action

//Open-close principle applies because it is open to the IWorker interface

//Interface segregation principle does not apply